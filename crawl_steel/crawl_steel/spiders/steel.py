from datetime import datetime
import scrapy
import csv
from scrapy.http import FormRequest
import json
from scrapy_playwright.page import PageMethod
from playwright.sync_api import Playwright, sync_playwright
# from crawl_steel.settings import COOKIES_FILE
import unicodedata
import happybase


class SteelSpider(scrapy.Spider):
    name = "steel"
    allowed_domains = ["giathepton.com"]
    start_urls = [
        "http://giathepton.com/api/public/article/search?siteId=4&to=20230516&subCategoryId=154&size=25&page=0"]
    billets = {}
    hrc = {}
    url_crawled = []
    # custom_settings = {
    #     "PLAYWRIGHT_LAUNCH_OPTIONS": {
    #         "proxy": {
    #             "server": "http://118.71.66.50:32650",
    #             "username": "admin",
    #             "password": "admin",
    #         },
    #     }
    # }

    def __init__(self, hbase_host=None, hbase_port=None, hbase_table=None, *args, **kwargs):
        kwargs.pop('_job')
        super().__init__(*args, **kwargs)
        self.hbase_host = hbase_host
        self.hbase_port = int(hbase_port)
        self.hbase_table = hbase_table
        self.connect_hbase()
        self.size_page = 25

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # page = response.meta["playwright_page"]
        # response = json.loads(response.css('pre ::text').get())
        response = response.json()
        if response['data']['content'] is None:
            yield

        totalPages = response['data']['totalPages']
        page = response['data']['pageable']['pageNumber']

        for d in response['data']['content'][10:12]:
            title = d['title']
            title = self.remove_accents(title)

            if "gia-phoi-thep" in title:
                yield scrapy.Request(f'http://giathepton.com/tin/{d["id"]}/{title}',
                                     meta={'playwright': True,
                                           "playwright_include_page": True,
                                           "playwright_context": "new",
                                           #    "playwright_page": page,
                                           'playwright_context_kwargs': {
                                               #    'storage_state': COOKIES_FILE,
                                               'storage_state': {
                                                   "origins": [
                                                       {
                                                           "origin": "http://giathepton.com",
                                                           "localStorage": [
                                                                {
                                                                    "name": "vss-account",
                                                                    "value": "{\"id\":15886,\"code\":\"false\",\"displayName\":\"Le Thi Nguyet\",\"username\":\"0336858312\",\"email\":\"lenguyetds@gmail.com\",\"mobile\":\"0336858312\",\"lastLogin\":1684846554436,\"siteId\":4,\"activated\":true,\"archived\":false,\"isModerator\":false,\"isAdmin\":false,\"isSale\":false,\"allowReadAll\":true,\"company\":\"DSC\",\"notes\":\"12/5: Kh\u00e1ch kh\u00f4ng l\u00e0m trong ng\u00e0nh, c\u00e1 nh\u00e2n v\u00e0o m\u1ea1ng t\u00ecm ki\u1ebfm \u0111\u1ecdc tin tham kh\u1ea3o qua loa, kh\u00f4ng c\u00f3 \u00fd \u0111\u1ecbnh m\u1edf t\u00e0i kho\u1ea3n \u0111\u1ecdc tin h\u00e0ng ng\u00e0y tr\u1ea3 ph\u00ed\",\"userType\":\"interested\",\"expiredDate\":1683883006000,\"createdDate\":1683369081000,\"interestedCategories\":\"Th\u00e9p\",\"roles\":[\"ROLE_USER\"]}"
                                                                },
                                                               {
                                                                    "name": "vtd-device-id",
                                                                    "value": "f393a0ce4192bdbfbd76c72acb1bfeab"
                                                                },
                                                               {
                                                                    "name": "vss-auth",
                                                                    "value": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsZW5ndXlldGRzQGdtYWlsLmNvbSIsImlkIjoxOTY3OTEsImRldmljZUlkIjoiNWQ1OWI4OTk0Y2YwOGIwNjM2Y2FjMzdmZDE0NGQxNjQiLCJwbGF0Zm9ybSI6MywiZXhwIjoxNjg1NzEzODc4fQ.9uOgCLDwmuqkhI8lF_WbVL7mJBuyMV6lyuZ_lw7flA_A5h4f-maFLbN0Jlcnc2qJMD5sf3HO1w8TgmFrA5_XTA"
                                                                }
                                                           ]
                                                       }
                                                   ]
                                               }
                                           },
                                           "playwright_page_methods":  [
                                               PageMethod(
                                                   "wait_for_selector", 'div.Wrapper__TimeSpan-fih67w-3'),
                                               PageMethod(
                                                   "wait_for_selector", 'div.content-block.Wrapper-fih67w-0.boElJU'),
                                               PageMethod(
                                                   "wait_for_load_state", "load"),
                                               #    PageMethod(
                                               #        "wait_for_timeout", "0")
                                           ],
                                           'download_timeout': 10
                                           }, callback=self.parse_news_daily_billet,
                                     errback=self.close_context_on_error,
                                     )
            elif "gia-ban-hrc-cua-dai-ly" in title:
                yield scrapy.Request(f'http://giathepton.com/tin/{d["id"]}/{title}',
                                     meta={'playwright': True,
                                           "playwright_include_page": True,
                                           'playwright_context_kwargs': {
                                               'storage_state': COOKIES_FILE,
                                           },
                                           #    "playwright_page": page,
                                           "playwright_page_methods":  [
                                               PageMethod(
                                                   "wait_for_selector", 'div.Wrapper__TimeSpan-fih67w-3'),
                                               PageMethod(
                                                   "wait_for_selector", 'div.content-block.Wrapper-fih67w-0.boElJU'),
                                               PageMethod(
                                                   "wait_for_load_state", "load"),
                                           ]
                                           }, callback=self.parse_news_daily_hrc,
                                     errback=self.close_context_on_error,
                                     )

        if page < totalPages:
            page = page + 1
            yield scrapy.Request(url=f'http://giathepton.com/api/public/article/search?siteId=4&to=20230516&subCategoryId=154&size={self.size_page}&page={page}', callback=self.parse)

    def parse_news_daily_billet(self, res):
        if res.css("div.access-denined").get():
            return
        self.url_crawled.append(res.request.url)
        time = res.css(
            'div.Wrapper__TimeSpan-fih67w-3 span ::text').extract_first()
        date_time = datetime.strptime(time, "%H:%M %d/%m/%Y")

        table = res.css('div.vtd-table-wrapper table')[0]
        spec = table.xpath('./tbody/tr[2]/td[1]/p[1]/span/text()').get()
        market_and_price_type = table.xpath(
            './tbody/tr[2]/td[2]/p[1]/span/text()').get()
        price = table.xpath('./tbody/tr[2]/td[3]/p[1]/span/text()').get()
        # (key, product, market, price, type_steel, time)
        self.billets[f'billet_{date_time.date()}'] = {
            'cf:category': 'billet',
            'cf:product': 'billet',
            'cf:spec': f'{spec}',
            'cf:market': f'{market_and_price_type}',
            'cf:price': f'{price}',
            'cf:created_at': f'{date_time.date()}'
        }
        # yield {
        #     'hrc': (f'billet_{date_time.date()}', f'billet', f'{market_and_price_type}',
        #             f'{price}', 'hrc', f'{date_time.date()}', f'{spec}')
        # }
        self.save_to_hbase(self.billets)

    def parse_news_daily_hrc(self, res):
        if res.css("div.access-denined").get():
            return
        self.url_crawled.append(res.request.url)

        time = res.css(
            'div.Wrapper__TimeSpan-fih67w-3 span ::text').extract_first()
        date_time = datetime.strptime(time, "%H:%M %d/%m/%Y")

        table = res.css('div.vtd-table-wrapper table')[0]
        product = None
        td_num = 0
        index = 0

        for tr in table.css('tbody tr')[1:]:
            td_num = tr.xpath('count(./td)').get()
            if float(td_num) == 1.0:
                product = tr.xpath('./td[1]/p[1]/span/strong/text()').get()
            else:
                index = index + 1
                spec = tr.xpath('./td[1]/p[1]/span/text()').get()
                if spec is None:
                    spec = tr.xpath('./td[1]/p[1]/text()').get()

                market_and_price_type = tr.xpath(
                    './td[2]/p[1]/span/text()').get()
                if market_and_price_type is None:
                    market_and_price_type = tr.xpath(
                        './td[2]/p[1]/text()').get()

                price = tr.xpath('./td[3]/p[1]/span/text()').get()
                if price is None:
                    price = tr.xpath('./td[3]/p[1]/text()').get()

                # (key, product, market, price, type_steel, time, specification)
                self.hrc[f'hrc_{date_time.date()}_{index}'] = {
                    'cf:category': 'hrc',
                    'cf:product': f'{product}',
                    'cf:spec': f'{spec}',
                    'cf:market': f'{market_and_price_type}',
                    'cf:price': f'{price}',
                    'cf:created_at': f'{date_time.date()}'
                }
                # yield {
                #     'hrc': (f'hrc_{date_time.date()}_{index}', f'{product}', f'{market_and_price_type}',
                #             f'{price}', 'hrc', f'{date_time.date()}', f'{spec}', td_num)
                # }
        self.save_to_hbase(self.hrc)

    async def close_context_on_error(self, failure):
        yield{
            'err': failure
        }
        # page = failure.request.meta["playwright_page"]
        # await page.context.close()

    def connect_hbase(self):
        self.connection = happybase.Connection(
            host=self.hbase_host, port=self.hbase_port)
        self.table = self.connection.table(self.hbase_table)

    def save_to_hbase(self, data):
        try:
            with self.table.batch(batch_size=10000) as b:
                for rowId, cf in data.items():
                    b.put(rowId, cf)
        except Exception as e:
            print(f"error{e}")

    def closed(self, reason):
        # Write the updated stock codes to CSV using feed export
        with open("out.csv", 'w', newline='') as csv_file:
            writer = csv.writer(csv_file)
            for element in self.url_crawled:
                writer.writerow([element])

    # function to beauty text to url
    def remove_accents(self, str):
        # Normalize the string to decomposed form (with separate combining characters)
        normalized_str = unicodedata.normalize('NFD', str)

        # Remove all combining characters (accents, diacritics, etc.)
        ascii_str = ''.join(
            c for c in normalized_str if not unicodedata.combining(c))

        ascii_str = ''.join(
            ['' if c == ':' or c == ',' else c for c in ascii_str])
        ascii_str = ''.join(['-' if not c.isalnum() or c ==
                            ':' else c for c in ascii_str])

        # Replace the Vietnamese letters đ and Đ with d and D respectively
        ascii_str = ascii_str.replace('đ', 'd').replace('Đ', 'D')
        return ascii_str.lower()

# run with cmd
# scrapy crawl steel -a hbase_host='localhost' -a hbase_port=9090 -a hbase_table='billet_hrc'

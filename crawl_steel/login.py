import json
from playwright.async_api import Playwright, async_playwright
import asyncio

async def main():
    async with async_playwright() as p:
        browser = await p.chromium.launch(headless=False, slow_mo=50)
        context = await browser.new_context()
        page = await context.new_page()
        await page.goto('http://giathepton.com/login')
        await page.fill('input[name="username"]', '0336858312')
        await page.fill('input[name="password"]', 'eL72qhx29rTE@Lt')
        
        await page.wait_for_timeout(500)
        
        await page.click('button[type=submit]')
       
        # Get the state of the context
        await page.wait_for_timeout(5000)
        
        state = await context.storage_state()

        if state:
            # Write the state to a file
            with open('state.json', 'w') as f:
                json.dump(state, f)
        else:
            print('State is empty.')
        
        await context.close()
        await browser.close()

asyncio.run(main())
CREATE OR REPLACE PROCEDURE convertToWetrade()
LANGUAGE plpgsql AS 
$$
begin 
	insert into wetrade.dsc_stock_price_hist(ticker, trading_date, openprice, closeprice, highestprice, lowestprice, totalvolume)
	select ticker, TO_TIMESTAMP(tradingdate::bigint), cast(open as numeric), cast(close as numeric), cast(high as numeric), cast(low as numeric), cast(volume as numeric)
	from stg.hbase_stock;
end
$$;
CALL convertToWetrade();
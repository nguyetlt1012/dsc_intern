import requests
from datetime import datetime
from pyspark.sql import SparkSession
import os
import csv

class CrawlerStock:
    def __init__(self, api, end_day, appName):
        self.API = api
        self.ENDTIME= int(datetime.strptime(end_day, "%d/%m/%Y").timestamp())
        self.STOCK_CODES = []
        self.data = []
        self.spark = SparkSession.builder.appName(appName)\
                .config("spark.jars", "driver/hbase-spark-1.0.1-SNAPSHOT_spark331_hbase2415.jar")\
                .config("spark.jars", " driver/hbase-shaded-mapreduce-2.1.2.jar")\
                .config('hbase.zookeeper.quorum', 'localhost')\
                .config('hbase.zookeeper.property.clientPort', '2181')\
                .getOrCreate()

    def call_api(self):
        self.getSymbols()
        for idx,row in enumerate(self.STOCK_CODES):
            res = requests.get(f'https://histdatafeed.vps.com.vn/tradingview/history?symbol={row["code"]}&resolution=1D&from={row["start_time"]}&to={self.ENDTIME}')
            self.parse_stock(res, row)
        self.save_to_hbase()
            
    
    def parse_stock(self, res, row):
        res = res.json()
        
        if (res['s'] == 'no_data'):
            pass
        
        # data = []
        for i in range(len(res["t"])):
            self.data.append((f'{row["code"]}_{res["t"][i]}',f'{res["t"][i]}', f'{res["o"][i]}', f'{res["h"][i]}', 
                         f'{res["l"][i]}', f'{res["c"][i]}', f'{res["v"][i]}'))
        # print(row['code'], len(self.data))
        # self.save_to_hbase(df)
    
    
    def save_to_hbase(self):
        df = self.spark.createDataFrame(self.data, ["id", "tradingdate", "open", "high", "low", "close", "volume"])
        
        df.show(10)
        df.write.format("org.apache.hadoop.hbase.spark")\
            .option("hbase.columns.mapping","id STRING :key, tradingdate STRING cf:tradingdate, \
                open STRING cf:open, high STRING cf:high, low STRING cf:low, close STRING cf:close, volume STRING cf:volume")\
            .option("hbase.namespace", "default")\
            .option("hbase.table", "stocks")\
            .option("hbase.spark.use.hbasecontext", False)\
            .save()
            
    def getSymbols(self):
        with open(os.path.abspath(os.getcwd()) +"/symbols.csv", "r") as f:
            reader = csv.reader(f)
            next(reader)
            for row in reader:
                if row[0].isdigit():
                    pass
                self.STOCK_CODES.append({'code': row[0], 'start_time': int(
                    datetime.strptime(row[1], "%Y").timestamp()), 'num': 0})
        
    def run(self):
        self.call_api()
        
crawler = CrawlerStock(api='', appName='crawl stock', end_day='6/4/2023')
crawler.run()
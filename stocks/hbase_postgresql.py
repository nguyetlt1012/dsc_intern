from pyspark.sql import SparkSession
from pyspark.sql.functions import split
spark = SparkSession.builder\
    .config("spark.app.name", "read data")\
    .config("spark.jars", "/opt/spark/jars/hbase-spark-1.0.1-SNAPSHOT_spark331_hbase2415.jar, /opt/spark/jars/hbase-shaded-mapreduce-2.1.2.jar")\
    .config("spark.jars", "/opt/spark/jars/htrace-core4-4.2.0-incubating.jar")\
    .config("spark.jars", "postgresql-42.6.0.jar")\
    .config("spark.jars", "hbase-shaded-mapreduce-2.1.2.jar")\
    .getOrCreate()
spark.conf.set("spark.sql.execution.arrow.pyspark.enabled", "true")
print("set")
df = spark.read.format("org.apache.hadoop.hbase.spark")\
    .option("hbase.columns.mapping", "id STRING :key, open STRING cf1:open, high STRING cf1:high, low STRING cf1:low, close STRING cf1:close, volume STRING cf1:volume, tradingdate STRING cf1:tradingdate")\
    .option("hbase.table", "stock")\
    .option("hbase.spark.use.hbasecontext", False)\
    .load()
split_col = split(df['id'], '_')
df1 = df.select('open', 'high', 'close', 'low', 'volume', 'tradingdate', split_col.getItem(0).alias('ticker'))


df1.write.format("jdbc")\
    .option("driver", "org.postgresql.Driver")\
    .mode("append")\
    .option("url", "jdbc:postgresql://localhost:5432/test_db")\
    .option("dbtable", "stg.hbase_stock")\
    .option("user", "root")\
    .option("password", "root")\
    .save()

df1.show(10)

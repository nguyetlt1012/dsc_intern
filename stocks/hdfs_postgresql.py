from pyspark.sql import SparkSession

spark = SparkSession.builder\
    .config("spark.app.name", "transform hdfs to stg")\
    .config("spark.jars", "postgresql-42.6.0.jar")\
    .getOrCreate()
spark.sparkContext.setLogLevel("WARN")


parDF1=spark.read.parquet("hdfs://172.23.0.3:9000/data/bronze/api/datafeed/stx_mrk_hnxstock/year=2023/month=04/day=07/stx_mrk_hnxstock-2023-04-07.parquet").select(
    "ticker", "closeprice", "highestprice", "lowestprice", "openprice", "totalmatchvolume", "tradingdate")
        
parDF2 = spark.read.parquet("hdfs://172.23.0.3:9000/data/bronze/api/datafeed/stx_mrk_hosestock/year=2023/month=04/day=07/stx_mrk_hosestock-2023-04-07.parquet").select(
    "ticker", "closeprice", "highestprice", "lowestprice", "openprice", "totalmatchvolume", "tradingdate")
        
parDF3 = spark.read.parquet("hdfs://172.23.0.3:9000/data/bronze/api/datafeed/stx_mrk_upcomstock/year=2023/month=04/day=07/stx_mrk_upcomstock-2023-04-07.parquet").select(
    "ticker", "closeprice", "highestprice", "lowestprice", "openprice", "totalmatchvolume", "tradingdate")

df = parDF1.union(parDF2).union(parDF3)

df.write.format("jdbc")\
    .option("driver", "org.postgresql.Driver")\
    .mode("append")\
    .option("url", "jdbc:postgresql://localhost:5432/test_db")\
    .option("dbtable", "stg.stock")\
    .option("user", "root")\
    .option("password", "root")\
    .save()
# print(df.count())
# df.printSchema()
# df.show(10)

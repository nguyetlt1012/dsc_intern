import scrapy
import datetime
from scrapy_playwright.page import PageMethod
import happybase


class InflationRateSpider(scrapy.Spider):
    name = "inflation_rate"
    allowed_domains = ["tradingeconomics.com"]
    start_urls = ["https://tradingeconomics.com/calendar#"]
    dates = None

    def __init__(self, start, end=None, hbase_host=None,hbase_table=None, *args, **kwargs):
        # kwargs.pop('_job')
        super().__init__(*args, **kwargs)
        self.hbase_host = hbase_host
        self.hbase_table = hbase_table
        self.hbase_port = 9090
        self.start = datetime.datetime.strptime(start, '%Y-%m-%d').date()
        self.connect_hbase()
        
        self.end = datetime.datetime.today().date() if end is None else datetime.datetime.strptime(end, '%Y-%m-%d').date()
        self.data = []
        self.current_index = 0
        
    def start_requests(self):
        self.dates = self.split_date(start_date=self.start, end_date=self.end, step=59)
        print(self.dates)
        yield scrapy.Request(url=self.start_urls[0], callback=self.parse,
                            meta={
                                'playwright': True,
                                "playwright_include_page": True,
                                "playwright_context": "awesome_context",
                                'playwright_context_kwargs': {
                                    'storage_state': {
                                        "cookies": self.setCookies(start=f'{self.dates[self.current_index][0]}', end=f'{self.dates[self.current_index][1]}')}
                                },
                                "playwright_page_methods":  [
                                    PageMethod(
                                        "wait_for_selector", 'table#calendar'),
                                    PageMethod(
                                        "wait_for_load_state", "load"),
                                ]
                            }
                        )


    def parse(self, response):
        data = {}
        for thead in response.css('table#calendar thead.table-header'):
            date = thead.xpath('./tr[1]/th[1]/text()').get()
            date = date.replace('\r\n', '').strip()
            date = datetime.datetime.strptime(date, '%A %B %d %Y').date()

            table = thead.xpath(
                './/following-sibling::tbody[1]')
            for tr in table.css('tr'):
                category = tr.xpath('./@data-category').get()
                if category == 'inflation rate':
                    actual = tr.css('td.calendar-item span#actual ::text').extract_first()
                    country = tr.xpath('./@data-country').get()
                    
                    data[f'{country}_{date}'] = {
                        'cf:category': category,
                        'cf:country': country,
                        'cf:rate': actual,
                        'cf:date': f'{date}'
                    }
                    # yield{
                    #     'category': category,
                    #     'actual': actual,
                    #     'country': country,
                    #     'date': date
                    # }
        
        self.save_to_hbase(data)
        if self.current_index + 1 < len(self.dates):
            self.current_index += 1
            context = f'new{self.current_index}'
            yield scrapy.Request(url=self.start_urls[0], callback=self.parse,
                                meta={
                                    'playwright': True,
                                    "playwright_include_page": True,
                                    "playwright_context": context,
                                    'playwright_context_kwargs': {
                                        'storage_state': {
                                            "cookies": self.setCookies(start=f'{self.dates[self.current_index][0]}', end=f'{self.dates[self.current_index][1]}')}
                                    },
                                    "playwright_page_methods":  [
                                        PageMethod(
                                            "wait_for_selector", 'table#calendar'),
                                        PageMethod(
                                            "wait_for_load_state", "load"),
                                    ]
                                }
                            ) 
       
       
    def connect_hbase(self):
        self.connection = happybase.Connection(
            host=self.hbase_host, port=self.hbase_port)
        self.table = self.connection.table(self.hbase_table)    


    def save_to_hbase(self, data):
        try:
            with self.table.batch(batch_size=10000) as b:
                for rowId, cf in data.items():
                    b.put(rowId, cf)
        except Exception as e:
            print(f"error{e}")
        
        

    def split_date(self, start_date, end_date, step):
        result = []
        current_date = start_date
        
        while current_date < end_date:
            next_date = current_date + datetime.timedelta(days=step)

            if next_date <= end_date:  # Kiểm tra nếu khoảng cách không vượt quá end_date
                result.append((current_date, next_date))
            else:
                result.append((current_date, end_date))
                break

            current_date = next_date + datetime.timedelta(days=1)
        return result

    def setCookies(self, start, end, countries='vnm,usa,kor,emu,tha,chn'):
        return [
            {
                "name": "cal-custom-range",
                "value": f"{start}|{end}",
                "path": "/calendar",
                "domain": ".tradingeconomics.com",
            }, {
                "name": "calendar-countries",
                "value": countries,
                "path": "/calendar",
                "domain": ".tradingeconomics.com",
            }, {
                "name": "cal-timezone-offset",
                "value": "420",
                "path": "/",
                "domain": ".tradingeconomics.com",
            },
        ]

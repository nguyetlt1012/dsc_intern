import happybase

connection = happybase.Connection('localhost', 9090)
connection.create_table('hrc_vn', {'cf': dict()})
connection.create_table('iron', {'cf': dict()})
connection.create_table('pangasius_farm', {'cf': dict()})
connection.create_table('pangasius_us', {'cf': dict()})
connection.create_table('billet_hrc', {'cf': dict()})
connection.create_table('stocks', {'cf': dict()})
connection.create_table('inflation_rate', {'cf': dict()})
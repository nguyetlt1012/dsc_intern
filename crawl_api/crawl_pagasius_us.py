import requests
import datetime
from pyspark.sql import SparkSession
from dateutil.relativedelta import relativedelta

class CrawlerPangasiusUS:
    def __init__(self, api, appName):
        self.API = api
        self.panga = None
        self.START_MONTH = None
        self.spark = SparkSession.builder.appName(appName)\
            .config('hbase.zookeeper.quorum', 'localhost')\
            .config('hbase.zookeeper.property.clientPort', '2181')\
            .getOrCreate()
    def call_api(self):
        res = requests.get(self.API)
        res = res.json()
        for d in res:
            if d['specie'] == 'Pangasius' and  d['nickname']=='panga_viet_allspec_us':
                self.panga = d
        self.START_MONTH = datetime.datetime(self.panga['firstYear'], 1, 1) + relativedelta(months=self.panga['firstPeriod'] - 1) 
        
        data = []
        for i in range(len(self.panga['prices'])):
            data.append((f'pangasius_{(self.START_MONTH + relativedelta(months=i)).strftime("%d/%m/%Y")}', f'{self.panga["prices"][i]}', 
                         str(self.panga['volumes'][i]), self.panga['frequency'], self.panga['specie'], 
                         self.panga['volume_unit'], self.panga['point_of_trade']))
        
        df = self.spark.createDataFrame(data, ["id", "price", "volume", "frequency", "specie", "volume_unit", "point_of_trade"])
        self.save_to_hbase(df)
    
    def save_to_hbase(self, df):
        df.show(10)
        df.write.format("org.apache.hadoop.hbase.spark")\
            .option("hbase.columns.mapping","id STRING :key, price STRING cf:name, volume STRING cf:volume, \
                frequency STRING cf:frequency, specie STRING cf:specie, volume_unit STRING cf:volume_unit,  point_of_trade STRING cf:point_of_trade")\
            .option("hbase.namespace", "default")\
            .option("hbase.table", "pangasius_us")\
            .option("hbase.spark.use.hbasecontext", False)\
            .save()
            
    def run(self):
        self.call_api()

crawler = CrawlerPangasiusUS(api='https://www.undercurrentnews.com/datasets/wwgRuBoBKdr3CYYEy6K7', appName='crawl Pangasius US history')
crawler.run()
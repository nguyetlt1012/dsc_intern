import requests
import datetime
from pyspark.sql import SparkSession
from dateutil.relativedelta import relativedelta

class CrawlerPangasiusUSDaily:
    def __init__(self, api, appName):
        self.API = api
        self.panga = None
        self.spark = SparkSession.builder.appName(appName)\
            .config('hbase.zookeeper.quorum', 'localhost')\
            .config('hbase.zookeeper.property.clientPort', '2181')\
            .getOrCreate()
    def call_api(self):
        res = requests.get(self.API)
        res = res.json()
        for d in res:
            if d['specie'] == 'Pangasius' and  d['nickname']=='panga_viet_allspec_us':
                self.panga = d
        last_period = datetime.datetime.strptime(self.panga['last_period'], "Month %m, %Y").date()
        
        data = []
        
        if last_period == datetime.today().replace(day=1).date():
            last_index = len(self.panga['prices']) - 1
            data.append((f'panga_{last_period}', f'{self.panga["prices"][last_index]}', 
                         str(self.panga['volumes'][last_index]), self.panga['frequency'], self.panga['specie'], 
                         self.panga['volume_unit'], self.panga['point_of_trade']))
        
        df = self.spark.createDataFrame(data, ["id", "price", "volume", "frequency", "specie", "volume_unit", "point_of_trade"])
        self.save_to_hbase(df)
    
    def save_to_hbase(self, df):
        df.show(10)
        df.write.format("org.apache.hadoop.hbase.spark")\
            .option("hbase.columns.mapping","id STRING :key, price STRING cf:price, volume STRING cf:volume, \
                frequency STRING cf:frequency, specie STRING cf:specie, volume_unit STRING cf:volume_unit,  point_of_trade STRING cf:point_of_trade")\
            .option("hbase.namespace", "default")\
            .option("hbase.table", "pangasius_us")\
            .option("hbase.spark.use.hbasecontext", False)\
            .save()
            
    def run(self):
        self.call_api()

crawler = CrawlerPangasiusUSDaily(api='https://www.undercurrentnews.com/datasets/wwgRuBoBKdr3CYYEy6K7', appName='crawl pangasius daily')
crawler.run()
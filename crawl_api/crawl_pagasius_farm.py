import requests
import datetime
from pyspark.sql import SparkSession

class CrawlerPangasiusFarm:
    def __init__(self, api, appName):
        self.API = api
        self.panga = None
        self.START_WEEK = None
        self.END_DAY = None
        self.spark = SparkSession.builder.appName(appName)\
            .config('hbase.zookeeper.quorum', 'localhost')\
            .config('hbase.zookeeper.property.clientPort', '2181')\
            .getOrCreate()
    def call_api(self):
        res = requests.get(self.API)
        res = res.json()
        for d in res:
            if d['specie'] == 'Pangasius' and  d['nickname']=='panga_viet_farmgate_1000_1200':
                self.panga = d
        self.START_WEEK = datetime.datetime(self.panga['firstYear'], 1, 1) + datetime.timedelta(weeks=self.panga['firstPeriod']-1)
        # self.END_DAY = datetime.datetime.strptime(self.panga['last_period'].replace("Day ", ""), "%j, %Y")
        
        data = []
        for i in range(len(self.panga['prices'])):
            data.append((f'pangasius_{(self.START_WEEK+datetime.timedelta(weeks=i)).strftime("%d/%m/%Y")}', f'{self.panga["prices"][i]}', 
                         self.panga['frequency'], self.panga['specie'], self.panga['currency'], 
                         self.panga['volume_unit'], self.panga['point_of_trade']))
        
        df = self.spark.createDataFrame(data, ["id", "price", "frequency", "specie", "currency", "volume_unit", "point_of_trade"])
        self.save_to_hbase(df)
    
    def save_to_hbase(self, df):
        df.show(10)
        df.write.format("org.apache.hadoop.hbase.spark")\
            .option("hbase.columns.mapping","id STRING :key, price STRING cf:name, volume STRING cf:volume, \
                frequency STRING cf:frequency, specie STRING cf:specie, currency STRING cf:currency, volume_unit STRING cf:volume_unit,  point_of_trade STRING cf:point_of_trade")\
            .option("hbase.namespace", "default")\
            .option("hbase.table", "pangasius_farm")\
            .option("hbase.spark.use.hbasecontext", False)\
            .save()
            
    def run(self):
        self.call_api()

crawler = CrawlerPangasiusFarm(api='https://www.undercurrentnews.com/datasets/wwgRuBoBKdr3CYYEy6K7', appName='crawl Pangasius farm history')
crawler.run()
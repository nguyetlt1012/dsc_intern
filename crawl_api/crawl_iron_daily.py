import requests
import datetime
from pyspark.sql import SparkSession


class Crawl_iron:

    def __init__(self, api, appName):
        self.API = api
        self.spark = SparkSession.builder.appName(appName)\
            .config("spark.jars", "driver/hbase-spark-1.0.1-SNAPSHOT_spark331_hbase2415.jar")\
            .config("spark.jars", " driver/hbase-shaded-mapreduce-2.1.2.jar")\
            .config('hbase.zookeeper.quorum', 'localhost')\
            .config('hbase.zookeeper.property.clientPort', '2181')\
            .getOrCreate()

    def call_api(self):
        res = requests.post(self.API,json={
            "dataInterval": '1',
            "dataNormalized": 'false',
            "dataPeriod": 'Day',
            "days": '5000',
            "realtime": 'false',
            "returnDateType": "ISO8601",
            "elements": [
                {
                    "Label": "49458232",
                    "Type": "price",
                    "Symbol": "27110161"
                },
                {
                    "Label": "7d029d15",
                    "Type": "volume",
                    "Symbol": "27110161"
                }
            ]
        })
        res = res.json()
        data = []
        # filter 
        for i in range(len(res['Dates'])):
            # data_price = res["Elements"][0]
            
            # get open, high, low, close, volume
            date_time = datetime.datetime.strptime(res["Dates"][i], '%Y-%m-%dT%H:%M:%S')
            if date_time.date() == datetime.datetime.today().date():
                for obj in res["Elements"][0]["ComponentSeries"]:
                    # print(obj['Type'])
                    type = obj['Type']
                    if type == 'Open':
                        price_open = obj['Values'][i]
                    elif type == 'High':
                        price_high = obj['Values'][i]
                    elif type == 'Close':
                        price_close = obj['Values'][i]
                    elif type == 'Low':
                        price_low = obj['Values'][i]                    
                volume = res["Elements"][1]["ComponentSeries"][0]["Values"][i]
                companyName = res["Elements"][0]["CompanyName"]
                # id, open, high, low, close, volume, companyName, day
                data.append((f'iron_{res["Dates"][i]}', f'{price_open}', f'{price_high}', f'{price_close}', f'{price_low}', f'{volume}', f'{res["Dates"][i]}', f'{companyName}'))  

        #convert to df
        df = self.spark.createDataFrame(data, ["id", "open", "high", "low", "close", "volume","day", "companyName"])
        self.save_to_hbase(df)
        
    # save to hbase     
    def save_to_hbase(self, df):
        # df.show(10)
        df.write.format("org.apache.hadoop.hbase.spark")\
            .option("hbase.columns.mapping","id STRING :key, open STRING cf:open, high STRING cf:high, \
                low STRING cf:low, close STRING cf:close, volume STRING cf:volume,  day STRING cf:day, companyName STRING cf:companyName")\
            .option("hbase.namespace", "default")\
            .option("hbase.table", "iron")\
            .option("hbase.spark.use.hbasecontext", False)\
            .save()
            
    def run(self):
        self.call_api()

# run code

crawl = Crawl_iron(api="https://markets.ft.com/data/chartapi/series", appName='crawl iron china daily')
crawl.run()

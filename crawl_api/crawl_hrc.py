import requests
import datetime
from pyspark.sql import SparkSession

class Crawl_hrc:
    
    def __init__(self, api, appName):
        self.API = api
        self.spark = SparkSession.builder.appName(appName)\
            .config("spark.jars", "driver/hbase-spark-1.0.1-SNAPSHOT_spark331_hbase2415.jar")\
            .config("spark.jars", " driver/hbase-shaded-mapreduce-2.1.2.jar")\
            .config('hbase.zookeeper.quorum', 'localhost')\
            .config('hbase.zookeeper.property.clientPort', '2181')\
            .getOrCreate()
        
        
    def call_api(self):
        res = requests.post(self.API, json={
            "currency": 'USD',
            "itemID": 5430,
            "priceType": "cnf",
            "assessment": "Monthly",
            "convertType": "Daily"
        }, headers={
            "Content-Type":'application/json',
            'Content-Length':'76',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
        })
        if res.status_code != 200: 
            print(f'Status code {res.status_code}')
            return
        res = res.json()
        data = []

        for point in res['data']['point']:
            data.append((f'hrc_{point[0]/1000}', f'{point[1]}', f'usd'))
        
        df = self.spark.createDataFrame(data, ["id", "price", "currency"])
        
        self.save_to_hbase(df)
    
    def save_to_hbase(self, df):
        df.show(10)
        df.write.format("org.apache.hadoop.hbase.spark")\
            .option("hbase.columns.mapping","id STRING :key, price STRING cf:open, currency STRING cf:currency")\
            .option("hbase.namespace", "default")\
            .option("hbase.table", "hrc_vn")\
            .option("hbase.spark.use.hbasecontext", False)\
            .save()


    def run(self):
        try:
            self.call_api()
        except Exception as e:
            print(e)
        

crawl = Crawl_hrc(api='https://www.steelmint.com/price/graphByCurrency', appName='hrc crawl')
crawl.run()
